/*
    Cú pháp truy xuất đến các thẻ trên giao diện
    1 thẻ trên giao diện trong js được gọi là 1 DOM: document Object Model
*/
//Thay đổi nội dung thẻ input#username
var tagUserName = document.getElementById('username');
tagUserName.value = 'cybersoft123';

//Thay đổi nội dung thẻ password
var tagPassword = document.getElementById('password');
tagPassword.value = '123456';

//Thay đổi hình của thẻ img#hinhAnh

var tagImg = document.getElementById('hinhAnh');
tagImg.src = './img/hinh2.jpeg';

//Thay đổi nội dung văn bản của thẻ div#noiDung
//Dựa vào thuộc tính innerHTML
// var tagDiv = document.getElementById('noiDung');
// tagDiv.innerHTML = 'Ahihi cybersoft 123';

/*
    Khai báo hàm trong javascript
    Cú pháp khai báo hàm
    function [ten_ham]() {
        //Liệt kê các đoạn code cần thực hiện
    }
    Lệnh gọi hàm
    [ten_ham]();
*/

function thayDoiNoiDung() {
    //Các câu lệnh thực thi khi gọi hàm khai báo giữa dấu {}
    var tagDiv = document.getElementById('noiDung');
    tagDiv.innerHTML = 'Ahihi cybersoft 123';
}
//Gọi hàm
// thayDoiNoiDung();
/*
    Bài tập 1: 
    Khi người dùng click vào nút login xây dựng hàm console.log tài khoản và mật khẩu ra tab console
*/


function handleLogin() {
    //B1: Lấy thông tin người dùng nhập vào
    var tagUserName = document.getElementById('username');
    var tagPassword = document.getElementById('password');
    /*
        Phím tắt nhắc lệnh 
            window: ctrl + space
            macbook: option + esc
    */
    //B2: Hiển thị trên console.log
    console.log('username: ', tagUserName.value);
    console.log('password: ', tagPassword.value);
    //B3: Gắn tên hàm vào nút login tại event onclick
}
/*
    Bài tập 2: 
    Xây dựng hàm xử lý chức năng khi người dùng bấm vào nút button#btnNhanEmDi thì nội dung của thẻ p#txt sẽ thay đổi thành 'Đã thay đổi rồi nhé !'
*/

function handleChangeText() {
    var tagP = document.getElementById('txt');
    tagP.innerHTML = 'Nội dung đã được thay đổi!';
}
/* ------------------ Thay đổi style (css) của thẻ html -------------------- */

/*
    Đối với các thuộc tính css có từ 2 từ trở lên
    background-color -> style.backgroundColor
*/
var tagPNoiDung = document.getElementById('txtNoiDung');
tagPNoiDung.style.color = 'red';
tagPNoiDung.style.backgroundColor = 'black';
tagPNoiDung.style.paddingTop = '15px';
tagPNoiDung.style.paddingBottom = '15px';
tagPNoiDung.style.border = '1px solid red';
tagPNoiDung.style.fontSize = '20px';

/*
    Bài tập 3: 
    Khi click vào nút button#btnChangeStyle -> thẻ p#tagP sẽ ẩn đi, thẻ h3#tagTitle sẽ được làm mở và có background color, cũng như sẽ có border 
*/

function changeStyle() {
    var tagTitle = document.getElementById('tagTitle');
    var tagP = document.getElementById('tagP');

    //Ẩn thẻ p 
    
    tagP.style.display = 'none';
    //Css cho thẻ h3#tagTitle
    tagTitle.style.opacity = '0.6';
    tagTitle.style.border = '1px solid #000';
    tagTitle.style.backgroundColor = 'orange';
    tagTitle.style.padding = '15px';
    tagTitle.style.fontSize = '30px';
    tagTitle.innerHTML = 'Học cybersoft';
}
/* 
    Bài tập 4: 
    Xây dựng tính năng cho 2 nút button bật đèn và tắt đèn
*/

function batDen() {
    var tagImg = document.getElementById('imgDen');
    tagImg.src = './img/pic_bulbon.gif';
    //Sử dụng className thay đổi css
    var btnBatDen = document.getElementById('btnBatDen');
    btnBatDen.className = 'active-button';
    var btnTatDen = document.getElementById('btnTatDen');
    btnTatDen.className = '';

    //active nút bật đèn, remove active tắt đèn
    // var btnBatDen = document.getElementById('btnBatDen');
    // btnBatDen.style.backgroundColor='black';
    // btnBatDen.style.color = 'yellow';
    // var btnTatDen = document.getElementById('btnTatDen');
    // btnTatDen.style.backgroundColor = 'white';
    // btnTatDen.style.color = 'black';
}

function tatDen() {
    var tagImg = document.getElementById('imgDen');
    tagImg.src = './img/pic_bulboff.gif';
    var btnBatDen = document.getElementById('btnBatDen');
    btnBatDen.className = '';
    var btnTatDen = document.getElementById('btnTatDen');
    btnTatDen.className = 'active-button';


    // var btnBatDen = document.getElementById('btnBatDen');
    // btnBatDen.style.backgroundColor='white';
    // btnBatDen.style.color = 'black';

    // var btnTatDen = document.getElementById('btnTatDen');
    // btnTatDen.style.backgroundColor = 'black';
    // btnTatDen.style.color = 'yellow';
}
