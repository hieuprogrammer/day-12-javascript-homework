//alert('nội dung'): Hiển thị ra hộp thoại của browser
function tinhTienTip(){
    // alert(123);
    //B1: Lấy thông tin người dùng nhập từ giao diện
    var tongTien = document.getElementById('tongTien').value;
    var phanTramTiep = document.getElementById('phanTramTip').value;
    var soNguoiTip = document.getElementById('soNguoi').value;
    //B2: Lập công thức tính tiền tip 

    var tipTrenNguoi = tongTien * phanTramTiep / 100 / soNguoiTip;

    //Bước 3: In ra trên giao diện tại vị trí h1#tienTipMoiNguoi
    // var tagH1 = document.getElementById('tienTipMoiNguoi');
    //.toFixed(n): Làm tròn sau dấu , n chữ số
    document.getElementById('tienTipMoiNguoi').innerHTML = tipTrenNguoi.toFixed(2) + ' $';
}


/*
    Cách 2 gán sự kiện của nút button cho 1 hàm lưu ý không có dấu đóng mở ngoặc
*/
//Dom đến nút button muốn gán sự kiện (event)
// document.getElementById('btnTinhTien2').onclick = tinhTienTip;

document.getElementById('btnTinhTien2').onclick = function (){ //Khai báo hàm nặc danh
    // alert(123);
    //B1: Lấy thông tin người dùng nhập từ giao diện
    var tongTien = document.getElementById('tongTien').value;
    var phanTramTiep = document.getElementById('phanTramTip').value;
    var soNguoiTip = document.getElementById('soNguoi').value;
    //B2: Lập công thức tính tiền tip 

    var tipTrenNguoi = tongTien * phanTramTiep / 100 / soNguoiTip;

    //Bước 3: In ra trên giao diện tại vị trí h1#tienTipMoiNguoi
    // var tagH1 = document.getElementById('tienTipMoiNguoi');
    //.toFixed(n): Làm tròn sau dấu , n chữ số
    document.getElementById('tienTipMoiNguoi').innerHTML = tipTrenNguoi.toFixed(2) + ' $';
}

/*  
    Cách 3 gán sự kiện của nút button linh động
*/
// document.getElementById('btnTinhTien3').addEventListener('click', tinhTienTip);
// document.getElementById('btnTinhTien3').addEventListener('mouseenter', tinhTienTip);
document.getElementById('btnTinhTien3').addEventListener('click', function () {
     // alert(123);
    //B1: Lấy thông tin người dùng nhập từ giao diện
    var tongTien = document.getElementById('tongTien').value;
    var phanTramTiep = document.getElementById('phanTramTip').value;
    var soNguoiTip = document.getElementById('soNguoi').value;
    //B2: Lập công thức tính tiền tip 
    var tipTrenNguoi = tongTien * phanTramTiep / 100 / soNguoiTip;
    //Bước 3: In ra trên giao diện tại vị trí h1#tienTipMoiNguoi
    // var tagH1 = document.getElementById('tienTipMoiNguoi');
    //.toFixed(n): Làm tròn sau dấu , n chữ số
    document.getElementById('tienTipMoiNguoi').innerHTML = tipTrenNguoi.toFixed(2) + ' $';
});

