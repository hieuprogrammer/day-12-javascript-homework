
document.getElementById('btnTaoButton').onclick = function () {
    //Tạo ra 1 thẻ = js
    /*
        document.createElement('ten_the');
        Khi tạo ra 1 thẻ chứa vào 1 biến thì biến đó giống hệt biến mà chúng ta thực hiện DOM
    */
    //Tạo button
    var buttonCybersoft = document.createElement('button');
    //Tạo class cho button
    buttonCybersoft.className = 'btn btn-danger';
    //Tạo nội dung cho button
    buttonCybersoft.innerHTML = 'button cybersoft';

    /*
        Đưa thẻ được tạo lên giao diện html.
         + Chọn 1 thẻ trên giao diện 
         + appendChild(the_tao) => Đưa lên giao diện
    */
    //Dom đến 1 thẻ có sẵn trên giao diện
    document.getElementById('content').appendChild(buttonCybersoft);

}


/*
    Bài tập 1 : Tạo thẻ html cho bài viết

    - Khi người dùng nhập liệu vào thẻ textarea#noiDungBaiViet sau đó bấm nút button#btnDangBai thì sẽ tạo ra 1 thẻ p có class in đậm có nội dung người dùng nhập. Và nội dung đó sẽ được hiển thị tại div#hienThiBaiViet.
*/

document.getElementById('btnDangBai').onclick = function() {
    //B1: Lấy thông tin từ người dùng
    var noiDungBaiViet = document.getElementById('noiDungBaiViet').value;
    //B2: Tạo ra thẻ p
    var tagPNoiDung = document.createElement('p');
    tagPNoiDung.className = 'font-weight-bold mt-1';
    tagPNoiDung.innerHTML = noiDungBaiViet;
    //B3: Hiển thị thông tin cho người dùng
    document.getElementById('hienThiBaiViet').appendChild(tagPNoiDung);
}